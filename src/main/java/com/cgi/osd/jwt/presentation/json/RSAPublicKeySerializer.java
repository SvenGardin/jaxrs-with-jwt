package com.cgi.osd.jwt.presentation.json;

import java.io.IOException;
import java.security.interfaces.RSAPublicKey;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

public class RSAPublicKeySerializer extends JsonSerializer<RSAPublicKey> {

    @Override
    public void serialize(RSAPublicKey key, JsonGenerator jgen, SerializerProvider provider) throws IOException {
	jgen.writeStartObject();
	jgen.writeStringField("modulus", key.getModulus().toString());
	jgen.writeStringField("publicExponent", key.getPublicExponent().toString());
	jgen.writeEndObject();
    }

}
