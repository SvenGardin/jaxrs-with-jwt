package com.cgi.osd.jwt.presentation;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

import com.cgi.osd.jwt.businesslogic.JwtBuilder;

/**
 * This class is responsible for adding the JWT token to all authorized responses.
 *
 */
@Provider
public class ResponseFilter implements ContainerResponseFilter {

    @Inject
    private JwtBuilder jwtBuilder;

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext)
	    throws IOException {
	if (isAuthorized(requestContext)) {
	    final List<Object> jwt = new ArrayList<>();
	    jwt.add(jwtBuilder.buildJWT(requestContext.getSecurityContext().getUserPrincipal().getName()));
	    responseContext.getHeaders().put("jwt", jwt);
	}
    }

    private boolean isAuthorized(ContainerRequestContext requestContext) {
	final Object authorized = requestContext.getProperty("authorized");
	if (authorized != null && (Boolean) authorized) {
	    return true;
	}
	return false;
    }
}
