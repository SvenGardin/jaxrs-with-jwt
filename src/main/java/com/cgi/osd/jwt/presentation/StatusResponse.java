package com.cgi.osd.jwt.presentation;

public class StatusResponse {

    private String result = "OK";

    public StatusResponse(String result) {
	this.result = result;
    }

    public String getResult() {
	return result;
    }

    public void setResult(String result) {
	this.result = result;
    }

}
