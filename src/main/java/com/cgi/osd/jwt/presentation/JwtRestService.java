package com.cgi.osd.jwt.presentation;

import java.security.interfaces.RSAPublicKey;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

import org.jose4j.lang.JoseException;

import com.cgi.osd.jwt.businesslogic.CryptoKeyProvider;
import com.cgi.osd.jwt.businesslogic.JwtBuilder;

/**
 *
 * This class is responsible for providing a REST services for JWT handling.
 */
@Path("auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class JwtRestService {

    @Context
    private SecurityContext securityContext;

    @Inject
    private JwtBuilder jwtBuilder;

    @Inject
    private CryptoKeyProvider cryptoKeyProvider;

    @Path("token")
    @GET
    public Response getJwt() throws JoseException {
	final String authenticatedUser = securityContext.getUserPrincipal().getName();
	final Response resp = Response.ok(new StatusResponse("OK"))
		.header("jwt", jwtBuilder.buildJWT(authenticatedUser)).build();
	return resp;
    }

    @Path("key")
    @GET
    public RSAPublicKey getPublicKey() {
	final RSAPublicKey key = cryptoKeyProvider.getPublicCryptoKey();
	return key;
    }

    @Path("alive")
    @GET
    public StatusResponse isAlive() {
	return new StatusResponse("Is alive");
    }

    @Path("test")
    @POST
    public StatusResponse testPublicKey(RSAPublicKey publicKey) {
	StatusResponse response;
	if (cryptoKeyProvider.getPublicCryptoKey().equals(publicKey)) {
	    response = new StatusResponse("public key valid");
	} else {
	    response = new StatusResponse("public key not valid");
	}
	return response;
    }

}
