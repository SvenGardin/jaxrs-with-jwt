package com.cgi.osd.jwt.presentation;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;


@ApplicationPath("/rest")
public class JaxRsActivator extends Application {

}
