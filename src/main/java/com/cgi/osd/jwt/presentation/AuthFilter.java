package com.cgi.osd.jwt.presentation;

import java.io.IOException;
import java.security.Principal;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Priority;
import javax.inject.Inject;
import javax.ws.rs.Priorities;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.ext.Provider;

import org.jose4j.jwt.JwtClaims;
import org.jose4j.jwt.MalformedClaimException;
import org.jose4j.jwt.consumer.InvalidJwtException;
import org.jose4j.jwt.consumer.JwtConsumer;
import org.jose4j.jwt.consumer.JwtConsumerBuilder;

import com.cgi.osd.jwt.businesslogic.CryptoKeyProvider;

@Priority(Priorities.AUTHENTICATION)
@Provider
@JwtSecure
public class AuthFilter implements ContainerRequestFilter {

    @Inject
    private Logger logger;

    @Inject
    private CryptoKeyProvider cryptoKeyProvider;

    private JwtConsumer jwtConsumer;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
	if (!hasJwtToken(requestContext) || !isJwtValid(requestContext)) {
	    logger.warning("Request missing or invalid JWT token");
	    requestContext.abortWith(Response.status(Response.Status.UNAUTHORIZED).build());
	} else {
	    requestContext.setProperty("authorized", true);
	    final String subject = getJwtSubject(requestContext);
	    final SecurityContext securityContext = requestContext.getSecurityContext();

	    requestContext.setSecurityContext(new SecurityContext() {
		@Override
		public String getAuthenticationScheme() {
		    return securityContext.getAuthenticationScheme();
		}

		@Override
		public Principal getUserPrincipal() {
		    return () -> {
			return subject;
		    };
		}

		@Override
		public boolean isSecure() {
		    return securityContext.isSecure();
		}

		@Override
		public boolean isUserInRole(String role) {
		    return securityContext.isUserInRole(role);
		}
	    });
	}
    }

    private String getJwt(ContainerRequestContext requestContext) {
	final String authHeaderVal = requestContext.getHeaderString("Authorization");
	final String[] authParts = authHeaderVal.split(":");
	if (authParts.length < 2 || !"jwt".equalsIgnoreCase(authParts[0])) {
	    return "";
	}
	return authParts[1].trim();
    }

    private String getJwtSubject(ContainerRequestContext requestContext) {
	String subject = "";
	try {
	    final String jwt = getJwt(requestContext);
	    final JwtClaims jwtClaims = jwtConsumer.processToClaims(jwt);
	    subject = jwtClaims.getSubject();
	} catch (final InvalidJwtException | MalformedClaimException e) {
	    logger.warning("JWT not valid");
	}
	return subject;
    }

    private boolean hasJwtToken(ContainerRequestContext requestContext) {
	return !getJwt(requestContext).isEmpty();
    }

    @PostConstruct
    private void init() {
	jwtConsumer = new JwtConsumerBuilder().setRequireSubject()
		.setVerificationKey(cryptoKeyProvider.getPublicCryptoKey()).build();
    }

    private boolean isJwtValid(ContainerRequestContext requestContext) {
	try {
	    final String jwt = getJwt(requestContext);
	    jwtConsumer.processToClaims(jwt);
	} catch (final InvalidJwtException e) {
	    logger.warning("JWT not valid");
	    return false;
	}
	return true;
    }

}
