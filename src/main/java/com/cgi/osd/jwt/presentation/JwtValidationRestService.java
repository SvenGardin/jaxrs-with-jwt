package com.cgi.osd.jwt.presentation;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

/**
 * This class provides a validation for the JWT token.
 *
 */
@Path("validate")
@Produces(MediaType.APPLICATION_JSON)
@JwtSecure
public class JwtValidationRestService {

    @GET
    public StatusResponse validate(@Context SecurityContext ctx) {
	return new StatusResponse(ctx.getUserPrincipal().getName());
    }
}
