package com.cgi.osd.jwt.presentation.json;

import java.io.IOException;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;

public class RSAPublicKeyDeserializer extends JsonDeserializer<RSAPublicKey> {

    @Override
    public RSAPublicKey deserialize(JsonParser parser, DeserializationContext context)
	    throws IOException, JsonProcessingException {
	final JsonNode node = parser.getCodec().readTree(parser);
	final BigInteger modulus = getBigInteger(node, "modulus");
	final BigInteger publicExponent = getBigInteger(node, "publicExponent");
	final RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulus, publicExponent);
	return getPublicKey(keySpec);
    }

    private BigInteger getBigInteger(JsonNode node, String name) throws JsonProcessingException, IOException {
	final String stringValue = ((TextNode) node.get(name)).asText();
	final BigInteger value = new BigInteger(stringValue);
	return value;
    }

    private RSAPublicKey getEmptyPublicKey() {
	RSAPublicKey publicKey = null;
	try {
	    final KeyFactory keyfactory = KeyFactory.getInstance("RSA");
	    publicKey = (RSAPublicKey) keyfactory
		    .generatePublic(new RSAPublicKeySpec(BigInteger.ZERO, BigInteger.ZERO));
	} catch (InvalidKeySpecException | NoSuchAlgorithmException e) {
	    // Won't happen here
	}
	return publicKey;
    }

    private RSAPublicKey getPublicKey(RSAPublicKeySpec keySpec) {
	RSAPublicKey publicKey = null;
	try {
	    final KeyFactory keyfactory = KeyFactory.getInstance("RSA");
	    publicKey = (RSAPublicKey) keyfactory.generatePublic(keySpec);
	} catch (final NoSuchAlgorithmException e) {
	    // Won't happen
	} catch (final InvalidKeySpecException e) {
	    publicKey = getEmptyPublicKey();
	}
	return publicKey;
    }

}
