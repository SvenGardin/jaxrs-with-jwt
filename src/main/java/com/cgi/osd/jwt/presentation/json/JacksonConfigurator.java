package com.cgi.osd.jwt.presentation.json;

import java.security.interfaces.RSAPublicKey;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

@Provider
public class JacksonConfigurator implements ContextResolver<ObjectMapper> {

    private final ObjectMapper objectMapper;

    public JacksonConfigurator() {
	objectMapper = new ObjectMapper();
	final SimpleModule module = new SimpleModule("JwtModule", Version.unknownVersion());
	module.addSerializer(RSAPublicKey.class, new RSAPublicKeySerializer());
	module.addDeserializer(RSAPublicKey.class, new RSAPublicKeyDeserializer());
	objectMapper.registerModule(module);
    }

    @Override
    public ObjectMapper getContext(Class<?> type) {
	return objectMapper;
    }

}
