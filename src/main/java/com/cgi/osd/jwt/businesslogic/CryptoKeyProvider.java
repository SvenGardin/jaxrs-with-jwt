package com.cgi.osd.jwt.businesslogic;

import java.security.Key;
import java.security.PrivateKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import org.infinispan.Cache;
import org.jose4j.jwk.RsaJsonWebKey;
import org.jose4j.jwk.RsaJwkGenerator;
import org.jose4j.lang.JoseException;

/**
 * This class is responsible for providing the cryptographic key.
 *
 */
@Singleton
@Lock(LockType.READ)
@TransactionManagement(TransactionManagementType.CONTAINER)
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public class CryptoKeyProvider {

    @Resource(lookup = "java:jboss/infinispan/container/jwt/crypto-key-cache")
    private Cache<String, Key> cryptoKeyCache;

    @Inject
    private Logger logger;

    private RSAPrivateKey privateCryptoKey;
    private RSAPublicKey publicCryptoKey;

    public PrivateKey getPrivateCryptoKey() {
	return privateCryptoKey;
    }

    public RSAPublicKey getPublicCryptoKey() {
	return publicCryptoKey;
    }

    @PostConstruct
    private void createCryptoKey() {
	final String privateKeyCacheKey = "privateRsaKey";
	final String publicKeyCacheKey = "publicRsaKey";
	try {
	    if (cryptoKeyCache.containsKey(privateKeyCacheKey)) {
		privateCryptoKey = (RSAPrivateKey) cryptoKeyCache.get(privateKeyCacheKey);
		publicCryptoKey = (RSAPublicKey) cryptoKeyCache.get(publicKeyCacheKey);
	    } else {
		final RsaJsonWebKey cryptoKey = RsaJwkGenerator.generateJwk(2048);

		privateCryptoKey = (RSAPrivateKey) cryptoKey.getPrivateKey();
		cryptoKeyCache.put(privateKeyCacheKey, privateCryptoKey, -1, TimeUnit.SECONDS);

		publicCryptoKey = (RSAPublicKey) cryptoKey.getKey();
		cryptoKeyCache.put(publicKeyCacheKey, publicCryptoKey, -1, TimeUnit.SECONDS);

		logger.info("New RSA key generated.");
	    }
	} catch (final JoseException e) {
	    final String errorMessage = "Failed to create RSA key: " + e.getMessage();
	    logger.log(Level.SEVERE, errorMessage);
	    throw new RuntimeException(errorMessage);
	}
    }

}
