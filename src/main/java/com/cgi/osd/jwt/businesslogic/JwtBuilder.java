package com.cgi.osd.jwt.businesslogic;

import java.util.logging.Logger;

import javax.inject.Inject;

import org.jose4j.jws.AlgorithmIdentifiers;
import org.jose4j.jws.JsonWebSignature;
import org.jose4j.jwt.JwtClaims;
import org.jose4j.lang.JoseException;

public class JwtBuilder {

    @Inject
    private CryptoKeyProvider cryptoKeyProvider;

    @Inject
    private Logger logger;

    public String buildJWT(String subject) {
	String jwt = "";
	try {
	    final JwtClaims claims = new JwtClaims();
	    claims.setSubject(subject);

	    final JsonWebSignature jws = new JsonWebSignature();
	    jws.setPayload(claims.toJson());
	    jws.setKey(cryptoKeyProvider.getPrivateCryptoKey());
	    jws.setAlgorithmHeaderValue(AlgorithmIdentifiers.RSA_USING_SHA256);
	    jwt = jws.getCompactSerialization();

	    logger.fine("Claim: " + claims);
	    logger.fine("JWS: " + jws);
	    logger.fine("JWT: " + jwt);

	} catch (final JoseException e) {
	    logger.severe("Faild to create JWT: " + e.getMessage());
	}
	return jwt;
    }
}
